Todo list

//Our task.
//Create a todo list with the following functions.
//
//1. showTodos 
//	Will return an array of all todos
//	
//2. addTodos(todo) -- done
//	Will add a new todo in our array of todos
//	
//3. updateTodo()
//	Given an index, will update the todo
//	
//4. deleteTodo()
//	Given an index, will delete a todo
//	

// 1. Create an empty todos array

const todos = [];

// Create a function that will add a new todo.
function addTodo(task){
	//Things to consider:
	//1. What is the data type of the todo data? String? Object?
	//2. What are the parameters that this function needs?
	
	const newTodo = task;

	//push the newTodo into our todos array.
	todos.push(newTodo);

	return "Successfully added " + newTodo;
}

function showTodos(){
	return todos;
}

function updateTodo(index, updatedTask){
	// Things to consider:
	// 1. When updating a data, we need something to identify which data we want to update. In this case, we will use the index.
	// 2. We need the updated value.
	todos[index]= updatedTask;
	return "Successfully updated task!";
}

function deleteTodo(index){
	//Things to consider:
	//1. When deleting a data, we need something to identify which data we want to delete.
	todos.splice(index, 1);
	return "Successfully deleted task!";
}