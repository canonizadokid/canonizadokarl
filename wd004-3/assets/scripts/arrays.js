// Array --- is a collection of related data. Denoted by [].
//Ex:

const fruits = ['Apple', 'Banana', 'Kiwi'];

const ages = [1,2,3,4];

const students = [ 
	{name: "brandon", age: 11}, 
	{name: "brenda", age: 12},
	{name: "celmer", age: 15},
	{name: "archie", age: 16}
];

//it is recommended to use plural form when naming an array, and singular when naming an object

//to access an array, we need to use bracket notation and the index of the item we want to access.
//
//index is the position of the data relative to the position of the first data.
//index always starts at 0.
//
//To add a data in an array,
//we will use the method .push();
//array.push(dataWeWantToAdd);
//the method will return the length of the array.
//length means # of data inside an array
//The new data will be added at the end.
//
//to update a data, access the data first via bracket notation and then re-assign a value.
//array[index] = newValue;
//
//to delete a data at the end of an array, we use the method pop();
//array.pop();
//It will return the deleted data;
//
//to delete a data at the start of an array, we use the method shift();
//array.shift();
//It will return the deleted data;
//
//To add a data in the beginning/ Start/ umpisa of an array, we use the method unshift(dataWeWantToAdd);
//array.unshift(dataWeWantToAdd);
//This iwll return the new length of the array
//
//push--- add / end
//pop --- delete / end
//shift --- delete / start
//unshift --- add / start
//
//.splice();
//It takes at least 2 arguments.
//array.splice( startingIndex, noOfItemsYouWantToDelete, --optional dataWeWantToInsert );
//This will return an array of deleted items;
//If the deleteCount is 0, it will just insert the new data from the starting index.