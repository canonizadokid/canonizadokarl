// Please set up your files.
// wd004-2

// Agenda:
// Review of Yesterday's Lesson
// Functions
// Objects
// 
// Variables:
// Let and Const
// 
// Data types:
// String - "", 
// Number - we can use arithmetic operators
// Boolean - true/ false
// 
// Arithmetic Operators:
// +, -, *, /, %
// 
// Modulo- remainder
// 
// Assignment Operator: =
// 
// A-A operators: +=, -=, *=, /= 
// 
// let number = 4;
// number += 4; //the value is 8
// number -= 2; // the value is 6
// number *= 2; // the value is 12
// number /= 3; // the value is 4

// const num1 = 2;
// const num2 = 4;

// const sum1 = num1 + num2;
// console.log(sum1);

// const num3 = 5;
// const num4 = 3;

// const sum2 = num3 + num4;
// console.log(sum2);

// const num5 = 11;
// const num6 = 3;

// const sum3 = num5 + num6;
// console.log(sum3);

// DRY Principle
// Don't Repeat Yourself
// 
//  Functions
//  subprogram that is designed to do a particular task 
//  
//  We need a function that will add two numbers
//  syntax:
// function nameOfTheFunction(){
// 	//task or what your function should do
// } 
// 
// remember, just like your variables, function names should be descriptive.
// function names/ variables should be in camel case.
// parameters - these are the values that the function needs in order to do its task
// Function declaration
// getSum(2,3);
// If the function does not have a return value, it will return an undefined data
function getSum(num1, num2){
	const sum = num1 + num2;
	console.log(sum);
	// return "I know the answer but I won't tell you";
}

// function call
// Arguments - these are the values that we will send to the function 
// getSum(2,3); //2 is num1, 3 is num2
// getSum(4,5);
// getSum(9,8);
// 
const ageSum = getSum(15, 18);
console.log(ageSum);

Activity Answer:


function getSum(num1, num2){
	const sum = num1 + num2;
	return sum;
}

function getDifference(num1, num2){
	const difference = num1 - num2;
	return difference;
}

function getProduct(num1, num2){
	const product = num1 * num2;
	return product;
}

function getQuotient(num1, num2){
	const quotient = num1/ num2;
	return quotient;
}


const pi = 3.1416; 
// pi r^2 h

function getCylinderVolume(radius, height){
	const radiusSquared = radius * radius;
	const piMultipliedByRadius = pi * radiusSquared;
	const volume = piMultipliedByRadius * height;

	// const radiusSquared = getProduct(radius, radius);
	// const piMultipliedByRadius = getProduct(pi, radiusSquared);
	// const volume = getProduct(piMultipliedByRadius, height);
	
	return volume;
}

//b1+b2/2 * h
function getTrapezoidArea(base1, base2, height){
	const sumOfBases = base1 + base2;
	//const sumOfBases = getSum(base1, base2);
	const halfOfBases = sumOfBases / 2;
	//const halfOfBases = getQuotient(sumOfBases, 2);
	const area = halfOfBases * height;
	//const area = getProduct(halfOfBases, height);
	return area;
}

function getTotalAmount(cost, quantity, discount, tax){
	// convert the discount into decimal
	const convertedDiscount = discount / 100;
	//convert tax into decimal
	const convertedTax = tax/ 100;
	//get the total amount without discount
	const amountWithoutDiscount = cost * quantity;
	//compute discount
	const discountRate = amountWihoutDiscount * convertedDiscount;
	//deduct discount
	const discountedAmount = amountWithoutDiscount - discountRate;
	//compute tax
	const taxRate = discountedAmount * convertedTax;
	//finally, add the tax to get the total amount;
	const totalAmount = discountedAmount + taxRate;

	return totalAmount;
}

function getTotalSalary(){

}


// Data Types:
// 1. String
// 2. Number
// 3. Boolean
// 4. undefined ---> functions without return value, variables that are declared but without assigned values;
// 5. null --> no value. //explicit, if the operation cannot find a value
// 6. Objects --> collection of related values. Denoted by {}. Presented in key-value pairs.
// 

//key or property key/ property name
const student = {
	name: "Brandon",
	studentNumber: "2014-15233",
	course: "BS Physics",
	college: "Science",
	department: "Physics",
	age: 22,
	isSingle: true,
	motto: "Time is gold",
	address: {
		stName: "Mahogany St.",
		brgy: "Poblacion",
		city: "Makati City",
		zipCode: "12345"
	},
	showAge: function (){
		console.log(student.age);
		return student.age;
	},
	addAge: function(){
		// add 1 to the age property of student
		student.age += 1;
		return "Successfully added age!";
	}
};

const student2 = {
	name: "Brenda"
}

// Anonymous function
// A function inside an object is also called "Method"

// To access a specific key/ property of an object, we will use the dot notation.
// if you use dot notation after an object that does not exist, it will cause an error.

//if we access a property that does not exist, it will return undefined;
//
// to add a property in an object, we will use dot notation and assign the value;

student.gender = "Male";

//to update the value of a property, we will use dot notation and assign a new value;

student.isSingle = false; 

//to delete a property, we will use the keyword delete then object.propertyName

delete student.studentNumber;