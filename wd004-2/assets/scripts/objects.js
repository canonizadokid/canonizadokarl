// Objects
// Data Types:
// 1. string
// 2. Number
// 3. Boolean
// 4. Undefined --> Functions w/o return value, variables
// that are declared but w/o assigned values;
// 5. null --> no value. //explicit, if the operation
// cannot find a value
// 6. Objects --> collection of related values.
// Denoted by {}. Presented in key-value pairs.
//

 // const money = null;

// key or property key/property name
 const student = {
 	name: "Brandon",
 	studentNumber: "2014-15233",
 	course: "BS Physics",
 	college: "Science",
 	department: "Physics",
 	age: 22,
 	isSingle: true,
 	motto: "Time is Gold",
 	address: {
 		stName: "san jose st.",
 		brgy: "Luna",
 		city: "Zambales",
 		zipcode: "2206"
 	},
 	showAge: function(){
 		console.log(student.age);
 		return student.age;
 	},
 	addAge: function(){
 		student.age += 1;
 		return "Successfully added age!";
 	}
 		// add 1 to the age property of student
 };

// Anonymous function
// A function inside an object is alsi called
// "Method"

// To access a specific key/ poperty of an oject
//, we will use the dot notation.
// if you use dot notation after an oject that
// does not exist, it will cause an error.

// if we access property that does not exist,
// it will return undefined
//
// to add a property in an object, we will use
// dot notation and assign the value;

student.gender = "Male";

// to update the value of a property, we will
// use dot notation and assign a new value;

student.isSingle = false;

// to delete a property, we will use the keyword
// delete then oject.propertyName

delete student.studentNumber;